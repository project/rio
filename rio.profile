<?php

/**
 * implements hook_install_configure_form_alter()
 */
function rio_form_install_configure_form_alter(&$form, &$form_state) {
  $form['site_information']['site_mail']['#default_value'] = 'webmaster@psegno.it';
  $form['admin_account']['account']['name']['#default_value'] = 'webmaster';
  $form['admin_account']['account']['mail']['#default_value'] = 'webmaster@psegno.it';
  $form['server_settings']['site_default_country']['#default_value'] = 'IT';
  $form['server_settings']['date_default_timezone']['#default_value'] = 'Europe/Rome';
  $form['update_notifications']['update_status_module']['#default_value'] = array(1);
}

/**
 * Change the final task to our task
 */
function rio_install_tasks_alter(&$tasks, $install_state) {
  $tasks['install_finished']['function'] = 'rio_install_finished';
}

/**
 * Installation task; perform final steps and display a 'finished' page.
 */
function rio_install_finished(&$install_state) {
  drupal_set_title(st('@drupal installation complete', array('@drupal' => drupal_install_profile_distribution_name())), PASS_THROUGH);
  $messages = drupal_set_message();
  $output = '<p>' . st('Congratulations, you installed @drupal!', array('@drupal' => drupal_install_profile_distribution_name())) . '</p>';
  $output .= '<p>' . (isset($messages['error']) ? st('Review the messages above before visiting <a href="@url">your new site</a>.', array('@url' => url(''))) : st('<a href="@url">Visit your new site</a>.', array('@url' => url('')))) . '</p>';

  module_disable(array('comment', 'color', 'rdf'));

  // Enable PHP mailer.
  if (module_load_include('inc', 'phpmailer', 'phpmailer.admin') && function_exists('phpmailer_settings_form_submit')) {
    $form_state = array(
      'values' => array(
        'smtp_on' => TRUE,
        'smtp_host' => 'localhost',
        'smtp_username' => '',
        'smtp_password' => '',
      ),
    );
    phpmailer_settings_form_submit(NULL, $form_state);
    system_settings_form_submit(NULL, $form_state);
  }

  // Set up the public file system.
  variable_set('file_public_path', 'files');
  $temp_dir = 'files/tmp';
  variable_set('file_temporary_path', $temp_dir);
  $temp_dir = realpath(DRUPAL_ROOT . '/' . $temp_dir);
  drupal_set_message($temp_dir);
  drupal_mkdir($path, NULL, TRUE);

  // Flush all caches to ensure that any full bootstraps during the installer
  // do not leave stale cached data, and that any content types or other items
  // registered by the install profile are registered correctly.
  drupal_flush_all_caches();

  // Remember the profile which was used.
  variable_set('install_profile', drupal_get_profile());

  // Install profiles are always loaded last
  db_update('system')
    ->fields(array('weight' => 1000))
    ->condition('type', 'module')
    ->condition('name', drupal_get_profile())
    ->execute();

  // Cache a fully-built schema.
  drupal_get_schema(NULL, TRUE);

  // Run cron to populate update status tables (if available) so that users
  // will be warned if they've installed an out of date Drupal version.
  // Will also trigger indexing of profile-supplied content or feeds.
  drupal_cron_run();

  return $output;
}
