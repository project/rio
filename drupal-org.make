api = 2
core = 7.x


; themes

projects[zen][type] = theme
projects[zen][version] = 5.1


; contrib

projects[admin_language][subdir] = contrib
projects[admin_language][version] = 1.x-dev

projects[admin_menu][subdir] = contrib
projects[admin_menu][version] = 3.0-rc4

projects[admin_views][subdir] = contrib
projects[admin_views][version] = 1.1

projects[better_formats][subdir] = contrib
projects[better_formats][version] = 1.0-beta1

projects[context][subdir] = contrib
projects[context][version] = 3.0-beta6

projects[context_addassets][subdir] = contrib
projects[context_addassets][version] = 1.0-beta1

projects[ctools][subdir] = contrib
projects[ctools][version] = 1.3

projects[entity][subdir] = contrib
projects[entity][version] = 1.1

projects[features][subdir] = contrib
projects[features][version] = 1.0

projects[fences][subdir] = contrib
projects[fences][version] = 1.0

projects[field_group][subdir] = contrib
projects[field_group][version] = 1.1

projects[globalredirect][subdir] = contrib
projects[globalredirect][version] = 1.5

projects[l10n_update][subdir] = devel
projects[l10n_update][version] = 1.0-beta3

projects[libraries][subdir] = contrib
projects[libraries][version] = 2.1

projects[module_filter][subdir] = contrib
projects[module_filter][version] = 1.7

projects[pathauto][subdir] = contrib
projects[pathauto][version] = 1.2

projects[phpmailer][subdir] = contrib
projects[phpmailer][version] = 3.x-dev

projects[redirect][subdir] = contrib
projects[redirect][version] = 1.0-rc1

projects[strongarm][subdir] = contrib
projects[strongarm][version] = 2.0

projects[taxonomy_display][subdir] = contrib
projects[taxonomy_display][version] = 1.1

projects[title][subdir] = contrib
projects[title][version] = 1.x-dev

projects[token][subdir] = contrib
projects[token][version] = 1.5

projects[transliteration][subdir] = contrib
projects[transliteration][version] = 3.1

projects[views][subdir] = contrib
projects[views][version] = 3.7

projects[views_bulk_operations][subdir] = contrib
projects[views_bulk_operations][version] = 3.1

projects[webform][subdir] = contrib
projects[webform][version] = 3.18


; custom

projects[mail_hijack][type] = module
projects[mail_hijack][download][type] = file
projects[mail_hijack][download][url] = http://download.psegno.it/files/rio/modules/mail_hijack.zip
projects[mail_hijack][subdir] = devel

projects[rio_core][type] = module
projects[rio_core][download][type] = file
projects[rio_core][download][url] = http://download.psegno.it/files/rio/modules/rio_core.zip
projects[rio_core][subdir] = custom/rio

projects[rio_ux][type] = module
projects[rio_ux][download][type] = file
projects[rio_ux][download][url] = http://download.psegno.it/files/rio/modules/rio_ux.zip
projects[rio_ux][subdir] = custom/rio

projects[rio_media][type] = module
projects[rio_media][download][type] = file
projects[rio_media][download][url] = http://download.psegno.it/files/rio/modules/rio_media.zip
projects[rio_media][subdir] = custom/rio

projects[rio_html_editor][type] = module
projects[rio_html_editor][download][type] = file
projects[rio_html_editor][download][url] = http://download.psegno.it/files/rio/modules/rio_html_editor.zip
projects[rio_html_editor][subdir] = custom/rio

libraries[jquery_mobilemenu][directory_name] = jquery.mobileMenu
libraries[jquery_mobilemenu][download][type] = get
libraries[jquery_mobilemenu][download][url] = http://download.psegno.it/files/rio/libraries/mobilemenu.zip
libraries[jquery_mobilemenu][destination] = libraries


; phpmailer

libraries[phpmailer][directory_name] = phpmailer
libraries[phpmailer][download][type] = get
libraries[phpmailer][download][url] = http://download.psegno.it/files/rio/libraries/phpmailer-5.2.4.zip
libraries[phpmailer][destination] = libraries
